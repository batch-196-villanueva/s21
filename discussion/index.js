//console.log("Hello, World");

//Arrays
//Arrays are used to store multiple related data/values in a single variable.
//It is created/declared using [] brackets also known as "Array Literals"

let hobbies = ["Play video games","Read a book","Listen to music"];

//Arrays make it easy to manage,manipulate a set of data. Arrays have different methods/functions that allow us to manage our array.
//Methods are functions associated with an object.
//Because Arrays are actually a special type of object.

console.log(typeof hobbies);

let grades = [75.4,98.5,90.12,91.50];
const planets = ["Mercury","Venus","Mars","Earth"];

//Arrays as a best practice contain values of the same type.

let arraySample = ["Saitama","One Punch Man", 25000, true];
console.log(arraySample);

//However, since there are no problems to creating arrays like this, you may encounter exceptions to this rule in the future and in fact in other JS libraries or framework.

//Array as a collection of data, has methods to manipulate and manage the array. Having values with different data types might interfere or conflict with the methods of an array.

/*
	Mini-Activity

	1. Create a variable which will store an array of at least 5 of your daily routine or task.

	2. Create a variable which will store at least 4 capital cities in the world.

	-Log the variables in the console and send a ss in our hangouts.

*/

let tasks = ["Brush teeth","Eat Breakfast","Go to Bootcamp","Eat Lunch","Play games"];

let capitalCities = [

	"Tokyo",
	"Ottawa",
	"Manila",
	"Hanoi"

];

console.log(tasks);
console.log(capitalCities);

//Each item in an array is called an element. 

//Array as a collection of data, as a convention, its name is usually plural.

//We can also add the values of variables as elements in an array.

let username1 = "fighter_smith1";
let username2 = "georgeKyle5000";
let username3 = "white_night";

let guildMembers = [username1,username2,username3];

console.log(guildMembers);

//.length property
//The .length property of an array tells about the number of elements in the array.
//It can actually also be set and manipulated.
//The .length property of an array is a number type.

console.log(tasks.length);
console.log(capitalCities.length);

//In fact, even strings have a .length property, which tells us the number of characters in a string. 
//strings are able to use some array methods and properties.
//whitespaces are counted as characters.

let fullName = "Randy Orton";
console.log(fullName.length);

//We can manipulate the .length property of an array. Being that .length property is a number that tells the total number of elements in an array, we can also delete the last item in an array by manipulating the .length property.

//reassign the value of the .length property by subtracting 1 to its current value:
tasks.length = tasks.length-1;
console.log(tasks.length);
console.log(tasks);

//We cannot yet delete a specific item in our array, however in the next sessions, we will learn of an array method which will allow us to do so. We can do it now, manually, but that would require us to create our own algorithm to solve that problem.

//We could also decrement the .length property of an array. It will produce the same result, that it will delete the last item in the array.

capitalCities.length--;
console.log(capitalCities);

//Can we do the same trick with a string?
//No.

fullName.length = fullName.length-1;
console.log(fullName);

//If we can shorten the array by updating the length property, can we also lengthen or add using the same trick?

let theBeatles = ["John","Paul","Ringo","George"];
theBeatles.length++;
console.log(theBeatles);

//Accessing the Elements of an Array
//Accessing array elements is one of the more common tasks we do with an array.
//This can be done through the use of array indices.
//Array elements are ordered according to index.

//In programming languages such as Javascript, arrays start from 0.
//The first element in an array is at index 0.

console.log(capitalCities);

//If we want to access a particular item in the array, we can do so with array indices. Each item are ordered according to their index. We can locate items in an array via their index. To be able to access an item in an array, first identify the name of the array, they add [] square brackets and then add the index number of your item.

//syntax: arrayName[index]

//Note: index are number types.

console.log(capitalCities[0]);

let lakersLegends = ["Kobe","Shaq","Lebron","Magic","Kareem"];
console.log(lakersLegends[1]);//Shaq
//What if we want to access Magic from the array?
console.log(lakersLegends[3]);//Magic

//We can also save/store a particular array element in a variable

let currentLaker = lakersLegends[2];
console.log(currentLaker);

//We can also update/reassign the array elements using their index.

lakersLegends[2]="Pau Gasol";
console.log(lakersLegends);

/*
	Mini-activity

	Update/reassign the last two items in the array with your own personal favorites.

	log the favoriteFoods array in the console with its updated elements.

*/

let favoriteFoods = [

	"Tonkatsu",
	"Adobo",
	"Hamburger",
	"Sinigang",
	"Pizza"

];

favoriteFoods[3] = "Beef Pares";
favoriteFoods[4] = "Lucky Me Pancit Canton";

console.log(favoriteFoods);

//Accessing the last item/element in an array

//How can we access the last item in our favorite foods array?
console.log(favoriteFoods[4]);

//What if we do not know the total number of items in the array?
//What if the array is constantly being added into?

//We could consistently access the last item in our array by accessing the index via adding the .length property value minus 1 as the index

//favoriteFoods[5-1] = favoriteFoods[4]

//the index to the last item is arrayName.length-1 because the index starts at 0
console.log(favoriteFoods[favoriteFoods.length-1]);

let bullsLegends = ["Jordan","Pippen","Rodman","Rose","Scalabrine","Kukoc","LaVine","Longley","DeRozan"];
console.log(bullsLegends[bullsLegends.length-1]);

//Add items in an array

//Using our indices we can also add items in our array.

let newArr = [];
console.log(newArr);
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Aerith Gainsborough";
console.log(newArr);

//WE can add items in our empty array, because we know the current total number of items in our array.
//So that we do not update the items in the array when we're adding instead, we can add items at the end of the array.

//the current last item of the array is accessed and updated instead.
//newArr[2-1] = newArr[1]
newArr[newArr.length-1] = "Tifa Lockhart";
console.log(newArr);

//newArr[2]
newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

//Looping Over an Array

//We can loop over an array and iterate all items in the array.
//Set the counter as the index and set a condition that as the current index iterated is less than the length of the array, we will run the loop.
//It is set this way because the index starts at 0

//Loop over and display all items in the newArr array:

for(let index = 0; index < newArr.length; index++){

	//So the current counter will be the index to access the item in the array.
	console.log(newArr[index])

}

let numArr = [5,12,30,46,40];

//Check each item in the array if they are divisible by 5 or not.
for(let index = 0; index < numArr.length; index++){

	//Loop over every item in the numArr array. The current iteration or the current loop number will be used as index.

	//first loop = index = 0 = numArr[index] = numArr[0]
	//2nd loop = index = 1 = numArr[index] = numArr[1]
	//...

	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	} else {
		console.log(numArr[index] + " is not divisible by 5");
	}
}

//Multidimensional Arrays

//Multidimensional Arrays are arrays that contain other arrays.

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);

//Accessing items in a multidimensional array is a bit different than 1 dimensional arrays.

//To access an item in an array within an array, first identify or locate the array where the item is.
//Add its location/index within its array:
console.log(chessBoard[1][5]);

//Mini-Activity:
//1. Log 'a8' in the console from our chessBoard multidimensional array:
console.log(chessBoard[7][0]);
//2. Log 'h6' in the console from our chessBoard multidimensional array:
console.log(chessBoard[5][7]);

//arrName[row][col] = arrName[y][x]
